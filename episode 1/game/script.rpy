﻿# Sie können das Skript Ihres Spiels in dieser Datei platzieren.

# Bestimmen Sie Grafiken unterhalb dieser Zeile, indem Sie die "Image-Statements" verwenden.
# z. B. image eileen happy = "eileen_happy.png"

# Bestimmen Sie Charaktere, die in diesem Spiel verwendet werden.
define j = Character('Jack', color="#c8ffc8", image="jack")
define p = Character('Peter', color='#bbeeff', image="peter")

define sonne = True

# Hier beginnt das Spiel.
label start:
    jump intro  

label intro:
    scene bg rathaus
    with fade

    "Tischstätten and der Mur."

    show jack
    j "... und damit schließen wir die heutige Ratssitzung."
    j "Oder so."
    j "Es ist super, Bürgermeister zu sein. Aber in einem Dorf mit nur einem Einwohner ist das ein viel zu harter Job."
    j "Wer führt Protokoll?"
    j "Ich."
    j "Wer bringt Kaffee und Kuchen zur Sitzung?"
    j "Ich."
    j "Wer muss abwaschen?"
    j "Ich."
    j "Da hat man ja gar keine Zeit mehr zum Regieren."
    j "Ich brauche wohl ein paar Mitbürger."
    j "Wie könnte man sie hierher locken?"

    "Einwohner: 1"
    show jack at left with dissolve
    show peter at right with dissolve

    p "Hallo Jack!"
    j "Hallo Peter!"
    j "Was führt dich nach Tischstätten?"
    p "Ich bin auf der Durchreise. Eigentlich wollte ich nach Bodendorf."
    p "Aber dann habe ich dich hier alleine am Platz stehen sehen."
    j "Ja... mir war langweilig."
    p "Hm. Wie geht es dir?"
    j "Gut. Was gibt es neues in Bodendorf?"
    p "Wieder mal ein Chaos-Monster."
    j "Stufe 4?"
    p "Stufe 2. Zwei Häuser hat's erwischt, aber die Teile sind unbeschädigt wieder aufgetaucht."
    j "Wie könnt ihr da unten nur so leben?"
    p "Tja. Viel Platz."
    j "Und viel Zerstörung."
    p "Ach, weißt eh. Solange es nicht permanent kaputt und unbrauchbar ist, ist es nur eine Veränderung."
    p "Jede Schöpfung bedingt auch eine Zerstörung."
    j "Ja, aber sag das mal dem Haus."
    p "Zumindest ist jede Zerstörung eine Chance für einen Neuanfang."
    j "Ja, dieses Credo kenne ich. Das Credo der Baumeister."
    p "Sonst wärest du ja auch nicht so steinreich."
    j "Da hast du recht. Ich verdiene mir jedes mal ein goldenes Näschen wenn da so ein Monster auftaucht."
    p "Wie viele Arbeiter hast du denn jetzt?"
    j "Franchise-Partner. Um die hundert, vermute ich."
    p "Kannst du dich noch erinnern, wie wir hier als Kinder herumgeturnt sind?"
    j "Wie kommst jetzt da drauf?"
    p "Du wolltest eigentlich immer eine Rebellion gegen deine Eltern anführen."
    j "Oh, ja. Damals."
    p "Weil du dachtest, man muss sich die Hände schmutzig machen, um zu verdienen, was man verdient."
    j "Ja, ich war naiv damals. Da habe ich nur das 'dienen' in 'verdienen' verstanden."
    p "Und jetzt?"
    j "Jetzt verstehe ich 'ver'."
    p "Ist mir zu hoch."
    j "'Von mir weg dienen'. Andere arbeiten lassen."
    p "Hm. Das ist jetzt deine Interpretation."
    j "Natürlich. Macht das einen Unterschied? Ich will ja nicht predigen."
    p "Hast du die restlichen Tischstätter verjagt?"
    j "Wohl kaum. Sieh dich selbst an. Du wolltest ja gar nicht weg hier."
    p "Stimmt. Ich wollte nicht weg, und du wolltest nicht bleiben. Und sieh uns jetzt an."
    j "Warum bist du dann gegangen?"
    p "Platz. Hier gibt es keinen Platz. Kannst du dich noch erinnern, wie wir da auf- und abgerast sind?"
    j "Auf unseren Go-Karts. Ja. Ich bin gelaufen so schnell ich konnte, und du hast nur 'schneller', 'schneller' geschrien."
    p "Ja, und versuch das mal mit einem Krankenwagen. Da ist hier gleich Schluss."
    p "Vielleicht solltest du dein 'Dorf' hier ein bisserl attraktiver machen?"
    
label menu_hospital:
    define menu_hospital_police = True
    menu:
        j "Was würde denn meine Stadt brauchen?"

        "Polizeistation" if menu_hospital_police:
            $ menu_hospital_police = False
            p "Aber wenn hier nichts los ist, wird die Polizei ziemlich unbeschäftigt sein."            
            jump menu_hospital

        "Krankenhaus":
            jump intro_hospital

label intro_hospital:
    p "Ja, das ist eine gute Idee."
    p "Ein privates Krankenhaus, dann kommen die Ärzte und wollen hier wohnen."
    j "Ja. Und Ärzte haben dann auch genug Geld zum Ausgeben."
    j "Dann kommt die restliche Infrastruktur automatisch"
    p "Und irgendwann hast du dann jemanden zum Protokoll schreiben."
    p "Die Grundstückspreise werden wohl steigen. Ich sollte mich gleich hier nieder lassen."

    "Einwohner: 2"

    p "Aber wie wollen wir das Vermarkten?"
    j "Siehst du das verfallene Häuserl da oben?"
    p "Da am Berg? Was ist damit?"
    j "Das ist eine alte Heiltherme. Die Kelten haben da oben schon ihre Kuren gemacht."
    p "Echt jetzt?"
    j "Jetzt ja."
    j "War damals ja auch schon da. Also der Bach da oben. Wieso sollten sie das nicht haben?"
    p "Ist das nicht Betrug?"
    j "Also hörmal, ich habe diese Geschichten schon in meiner Jugend gehört."
    j "Ich bin ja kein Archeologe."
    p "Hm.. dann richten wir zuerst mal das da oben am Hügel her."
    
    jump jacks_haus

label jacks_haus:
    scene bg construction
    with fade
    "Etwas später"

    show peter with fade
    p "Oh, beste Lage. Hier komme ich gleich nach Bodendorf."

    show peter at left 
    show jack at right with fade
    j "Hast du dich schon eingewöhnt?"
    p "Ja, hab ich. Also, wie ist die Geschichte zum Häuserl oben?"


label menu_tempel:
    define menu_tempel_schatz = True
    define menu_tempel_hotspring = True
    menu:
        j "Wie wär es damit:"

        "Schatz" if menu_tempel_schatz:
            $ menu_tempel_schatz = False
            j "Die Römer haben einen Schatz versteckt."
            p "Hm. Wie passt das zum Krankenhaus?"
            jump menu_tempel

        "Heißes Heilwasser" if menu_tempel_hotspring:
            $ menu_tempel_hotspring = False
            j "Dort oben ist eine Thermalquelle."
            p "Echt jetzt?"
            j "Nein. Nur ein Bach."
            jump menu_tempel

        "Tempel":
            j "Es war eine antike Gebetsstätte. Oder zumindest irgend ein anderer Ritual-Ort."
            jump bach

label bach:
    menu:
        p "Hm. Wie passt das zum Krankenhaus?"

        "Bach":
            j "Der Bach dort oben hat schon viele Wunder bewirkt."
            jump ende
        
        "Sonne" if sonne:
            $ sonne = False
            j "Dort hat man einen tollen Überblick, und an einem bestimmten Tag, wenn die Sonne richtig steht, kann man eine Höhle entdecken."
            p "Welche Höhle?"
            j "Es sind ja immer irgendwo welche Höhlen."
            p "An welchem Tag?"
            j "Weiß ich nicht."
            jump bach

label ende:
    scene bg construction
    ".:. Ende."
    return
